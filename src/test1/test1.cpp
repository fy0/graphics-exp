#include "share/base.h"

using gexp::g_window;
using gexp::g_render;

void dda_line(int x0, int y0, int x1, int y1)
{
	int x;
	float dx,dy,y,k;

	dx = x1 - x0;
	dy = y1 - y0;
	k = dy / dx;
	y = y0;

	for(x = x0;x<=x1;x++) {
		y = y + k;
		gexp::draw_pixel(x, int(y+0.5));
	}
}

void midpoint_line(int x0, int y0, int x1, int y1)
{
	int a,b,d1,d2,d,x,y;
	a = y0 - y1,b = x1-x0,d = 2*a+b;
	d1 = 2*a,d2 = 2*(a+b);
	x = x0,y = y0;
	gexp::draw_pixel(x, y);

	while(x<x1) {
		if(d<0) {
			x++,y++,d+=d2;
		} else {
			x++,d+=d1;
		}
		gexp::draw_pixel(x, y);
	}
}

void draw()
{
	gexp::set_color(0xff, 0, 0, 0xff);
	dda_line(0,0, 250, 200);
	gexp::set_color(0, 0, 0xff, 0xff);
	midpoint_line(0,0, 300, 150);
}

int main(int argc, char* argv[])
{
	gexp::init();
	gexp::clear();

	draw();

	gexp::mainloop();
 
    return 0;
}
