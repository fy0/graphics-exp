
#pragma once

#include <SDL.h>

namespace gexp {

static SDL_Window * g_window = NULL;
static SDL_Renderer* g_render = NULL;

int init();
void mainloop();
void clear();

void set_color(Uint8 r, Uint8 g, Uint8 b, Uint8 a);
void draw_pixel(int x, int y);

};
