
#include "base.h"

namespace gexp {

int init()
{
    if(SDL_Init(SDL_INIT_EVERYTHING) == -1)
        return(1);

	gexp::g_window = SDL_CreateWindow("test1", SDL_WINDOWPOS_CENTERED,  
            SDL_WINDOWPOS_CENTERED, 800, 600,
            SDL_WINDOW_SHOWN);

    gexp::g_render = SDL_CreateRenderer(g_window, -1,  
            SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);  
	return 0;
}

void mainloop()
{
	SDL_Event e;

	SDL_RenderPresent(g_render);
	while(SDL_WaitEvent(&e)) {
		switch(e.type) {
			case SDL_QUIT:
				return;
		}
	}

	SDL_Quit();
}

void clear()
{
	SDL_SetRenderDrawColor(g_render, 0xff, 0xff, 0xff, 0xff);
	SDL_RenderClear(g_render);
}

void set_color(Uint8 r, Uint8 g, Uint8 b, Uint8 a)
{
	SDL_SetRenderDrawColor(g_render, r, g, b, a);
}

void draw_pixel(int x, int y)
{
	SDL_RenderDrawPoint(g_render, x, y);
}

};
