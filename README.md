#graphics-exp

##下载

* 点击左上角的 ZIP 按钮


##使用

* 一、 解压ZIP到某个目录，比如 D:\codes\graphics-exp

* 二、 下载安装 CMake 记得安装时勾上 Add To Path(添加到Path环境变量，**重要**)

* 三、 打开cmd，输入：

    	cd D:\codes\graphics-exp\build
		D:
		cmake ..

* 四、 打开生成的工程文件，就可以使用了


## FAQ

* Q: 编译后文件在哪？
> A: 编译后的文件在 D:\codes\graphics-exp\build\Debug 或 Release 下

* Q: 执行exe提示找不到 SDL2.dll？
> A: 这个文件在graphics-exp\lib\SDL2-2.0.3\lib\x86下，复制一个到Debug目录就行

* Q: 啥是 cmake ? 为啥要用？
> A: 之所以用 cmake 是因为它可以自动生成从 VC2008 到 VC2013 全部的项目文件，还能生成Eclipse项目文件、Makefile等，也就是能够跨全平台编译。


## 其他

* 其他一些事情可以看一下 CMakeLists.txt 还有一段话

* 与时俱进，看情况更新
